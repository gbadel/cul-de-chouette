#include <iostream>
#include <vector>
#include <array>
#include <limits>
#include <cmath>
using namespace std;

struct Player {
	string nom;
	int score;
	bool grelottine = false;
	bool civet = false;
	bool passe_grelot = false;
};

struct Jeu {
	vector<Player> joueurs;
	size_t joueur_actuel;
	size_t lanceur;
	
	bool grelottineEnCours;
	bool sirotageEnCours;
	bool civetEnCours;
};

typedef array<unsigned int, 3> Dice;

enum Combinaison {Chouette, Velute, ChouetteVelute, CulDeChouette, SiropGrelot, Suite, SuiteVelute, Artichette, Soufflette, BleuRouge, Pelican, Flan, Neant};

Combinaison tour(Jeu& jeu);
Combinaison effet_combinaison(Jeu& jeu, Dice& dice);

/*
 *  Création du tableau des jouers.
 *  Demande des noms successifs des joueurs.
 *  Tous les joueurs sont initialisés avec score de 0 et aucun token.
 */
 
vector<Player> demander_joueurs() {
	vector<Player> joueurs = {};
	cout << "Entrer le nom des joueurs, dans l'ordre où ils vont jouer." << endl;
	
	Player joueur;
	string nom;
	int count = 1;
	do{
		cout << "Nom du joueur " << count << " ('end' pour terminer) : ";
		cin >> nom;
		
		if(nom != "end") {
			joueur.nom = nom;
			joueur.score = 0;
			joueur.grelottine = false;
			joueur.civet = false;
			joueur.passe_grelot = false;
			joueurs.push_back(joueur);
		}
		
		count++;
	}while(nom != "end");
	
	return joueurs;
}

/*
 *  Impression de l'état des scores et des tokens de tous les joueurs.
 *  Chaque joueur est imprimé au format
 * 		(>)Nom |(g)(p)(c)| Score
 * 	Le chevron '>' indique le joueur dont c'est le tour, les lettres 'g','p','c' signalent les tokens que possèdent ce joueur.
 */ 

void imprimer(const Jeu& jeu) {
	vector<Player> joueurs = jeu.joueurs;
	size_t N = joueurs.size();
	
	unsigned int longest_name = 0;										// Calcul du plus long nom
	unsigned int L = 0;
	for(size_t i=0; i<N; i++) {
		L = joueurs[i].nom.length();
		if(L > longest_name) {
			longest_name = L;
		}
	}
	
	cout << endl;
	cout << "+-----";
	for(unsigned int j=1; j<=longest_name; j++) {
		cout << "-";
	}
	cout << "--+---+------+" << endl;
	
	for(size_t i=0; i<N; i++) {											// Impression de chaque joueur sur une ligne
		L = joueurs[i].nom.length();
		cout << "| ";
		if(jeu.joueur_actuel == i)
			cout << ">";
		else
			cout << " ";
			
		cout << (i+1) << ". " << joueurs[i].nom; 
		for(unsigned int j=L; j<=longest_name; j++) {
			cout << " ";
		}
		cout << " |" << (joueurs[i].grelottine ? "g" : " ") << (joueurs[i].passe_grelot ? "p" : " ")
			 << (joueurs[i].civet ? "c" : " ") << "| " << (joueurs[i].score >= 0 ? " " : "") << joueurs[i].score;
		if(abs(joueurs[i].score) <= 99) cout << " ";
		if(abs(joueurs[i].score) <= 9) cout << " ";
		
		cout << " |" << endl;
	}
	
	cout << "+-----";
	for(unsigned int j=1; j<=longest_name; j++) {
		cout << "-";
	}
	cout << "--+---+------+" << endl;
	
	cout << endl;
}

/*
 * Fait passer la main au joueur suivant.
 */
void joueur_suivant(Jeu& jeu) {
	size_t N = ++jeu.joueur_actuel;
	if(N == jeu.joueurs.size())
		jeu.joueur_actuel = 0;
}
size_t joueur_suivant(size_t actuel, size_t N) {
	actuel++;
	if(actuel >= N)
		actuel = 0;
	
	return actuel;
}

/*
 * Trie les dés
 */
Dice sort(Dice dice) {
	unsigned int temp = 0;
	if(dice[0] > dice[1]) {
		temp = dice[1];
		dice[1] = dice[0];
		dice[0] = temp;
	}
	if(dice[1] > dice[2]) {
		temp = dice[2];
		dice[2]=dice[1];
		dice[1]=temp;
		
		if(dice[0] > dice[1]) {
			temp = dice[1];
			dice[1] = dice[0];
			dice[0] = temp;
		}
	}
	return dice;
}

/*
 * Détermine si un défi a été réussi, si la combinaison obtenue est suffisante pour cela
 */

bool defiReussi(Combinaison defi, Combinaison result) {
	switch(defi) {
		case Chouette :
			return result == Chouette || result == ChouetteVelute || result == CulDeChouette || result == SiropGrelot;
		case Velute :
			return result == Velute || result == ChouetteVelute || result == SuiteVelute;
		case ChouetteVelute :
			return result == ChouetteVelute;
		case CulDeChouette :
			return result == CulDeChouette || result == SiropGrelot;
		case SiropGrelot :
			return result == SiropGrelot;
		case Suite : 
			return result == Suite || result == SuiteVelute;
		case Neant :
			return result == Neant;
		default:
			return false;
	}
}

/*
 * Gestion des erreurs si l'utilisateur doit entrer un entier et qu'il entre des lettres
 */
int safe_get_int() {
	int I = 0;
	cin >> I;
	if(cin.fail()) {
		cin.clear();
		cin.ignore(numeric_limits<streamsize>::max(), '\n');
		I = 0;
	}
	return I;
}

/*
 * Quand on doit désigner un joueur par son numéro, cette fonction demande tant qu'on donne un numéro trop grand.
 */

unsigned int designer(size_t N) {
	unsigned int J = 0;
	do {
		if(J!=0)
			cout << "Ce n'est pas un numéro de joueur valide ! ";
		J = safe_get_int();
	} while(J > N);
	return J;
}

/*
 * Annoncer une combinaison (pour un défi ou une action)
 */

Combinaison annonceCombinaison() {
	cout << "Quelle combinaison est annoncée (C, V, CV, CDC, SG, S, N) ? ";
	string annonce = "";
	cin >> annonce;
	Combinaison combi = Neant;
	
	if(annonce == "C")
			combi = Chouette;
	else if(annonce == "V")
			combi = Velute;
	else if(annonce == "CV")
			combi = ChouetteVelute;
	else if(annonce == "CDC")
			combi = CulDeChouette;
	else if(annonce == "SG")
			combi = SiropGrelot;
	else if(annonce == "S")
			combi = Suite;
	else if(annonce == "N")
			combi = Neant;
	
	return combi;
}

/*
 * Attribution ou retrait de points à un joueur
 */
 
void donnerPts(Jeu& jeu, size_t joueur, int diff) {
	if(diff>=0) {
		cout << jeu.joueurs[joueur].nom + " gagne " + to_string(diff) + " points." << endl;
	} else {
		cout << jeu.joueurs[joueur].nom + " perd " + to_string(abs(diff)) + " points." << endl;
	}
	
	jeu.joueurs[joueur].score += diff;
}

/*
 * Bévue
 */

void bevue(Jeu& jeu) {
	cout << "Qui a commis une Bévue ? ";
	unsigned int perdant = designer(jeu.joueurs.size());
	if(perdant != 0) {
		donnerPts(jeu, perdant-1, -10);
	}
}

/*
 * Correction manuelle des scores si le programme (ou l'utilisateur) a commis une erreur
 */
void correction(Jeu& jeu) {
	cout << "Le score de qui faut-il corriger ? ";
	unsigned int J = designer(jeu.joueurs.size());
	if(J != 0) {
		cout << "De combien corriger le score de " << jeu.joueurs[J-1].nom << " ? ";
		int quant;
		quant = safe_get_int();
		jeu.joueurs[J-1].score += quant;
	}
}

//! ACTIONS ###############################################################################################

/*
 * Action qui se déroule si un joueur tente un Sirop ou un Attrape-Ouiseau.
 */

Combinaison sirotage(Jeu& jeu, Dice& dice,unsigned int val) {
	cout << "Qui tente un Sirop ou un Attrape-Oiseau ? ";
	jeu.sirotageEnCours = true;
	size_t N = jeu.joueurs.size();
	size_t siroteur = designer(N);
	if(siroteur != 0) {
		siroteur--;
		size_t miseur = joueur_suivant(siroteur, N);
		vector<unsigned int> mises(N,0);
		unsigned int mise = 0;
		
		cout << "Rappel des noms d'oiseaux : 1/ Linotte      4/ Mouette" << endl
			 << "                            2/ Alouette     5/ Bergeronnette" << endl
			 << "                            3/ Fauvette     6/ Chouette" << endl
			 << "Entrer 0 pour couche-sirop." << endl;
			 
		while(miseur != siroteur) {
			cout << "Que mise " << jeu.joueurs[miseur].nom << " ? ";
			mise = safe_get_int();
			mises[miseur] = mise;
			miseur = joueur_suivant(miseur,N);
		}
		
		unsigned int cible = val;
		cout << endl << jeu.joueurs[siroteur].nom << " tente un sirop de " << cible << " ! " << endl;
		cout << "Résultat du lancer : ";
		unsigned int jet = safe_get_int();
		
		cout << endl;
		if(jet == cible) {										//! Faux, gagne ou perd autres points.
			cout << "Sirop gagnant ! ";
			donnerPts(jeu, siroteur, 10*jet+40);
		} else {
			cout << "Sirotage Raté ! ";
			donnerPts(jeu, siroteur, -val*val);
			if(cible==6) {
				cout << jeu.joueurs[siroteur].nom + " a un civet." << endl;
				jeu.joueurs[siroteur].civet = true;
			}
		}
		miseur = joueur_suivant(siroteur,N);
		while(miseur != siroteur) {
			if(mises[miseur] == jet) {
				donnerPts(jeu,miseur, 25);
			} else if(mises[miseur] != 0) {
				donnerPts(jeu,miseur, -5);
			}
			miseur = joueur_suivant(miseur,N);
		}
		
		dice = sort({val,val,jet});
		
		
		Combinaison result = effet_combinaison(jeu,dice);
		jeu.sirotageEnCours = false;
		
		return result;
	} else {
		jeu.sirotageEnCours = false;
		return Chouette;
	}
}

/*
 * Défi grelottine
 */
void grelottine(Jeu& jeu) {
	cout << "Grelottine ! Qui lance le défi ? ";
	jeu.grelottineEnCours = true;
	unsigned int grelottin = designer(jeu.joueurs.size());
	
	if(grelottin != 0 && jeu.joueurs[grelottin-1].grelottine && jeu.joueurs[grelottin-1].score > 0)
	{
		cout << "Qui est défié ? ";
		unsigned int defie = designer(jeu.joueurs.size());
		
		if(defie != 0 && defie != grelottin && jeu.joueurs[defie-1].grelottine && jeu.joueurs[defie-1].score > 0)
		{
			unsigned int ptsTot = min(jeu.joueurs[defie-1].score , jeu.joueurs[grelottin-1].score);
			cout << endl << "Choisir le défi et le nombre de points en jeu. Maximum permis :" << endl
				 << "Chouette : " << ceil(ptsTot /3.) << endl
				 << "Velute : " << ceil(ptsTot /4.) << endl
				 << "Cul-de-Chouette : " << ceil(ptsTot /8.) << endl
				 << "Chouette-Velute : " << ceil(ptsTot /16.) << endl
				 << "Sirop-Grelot : " << ceil(ptsTot /32.) << endl;
				 
			Combinaison combiMise = Neant;
			while( combiMise != Chouette && combiMise != Velute && combiMise != CulDeChouette && combiMise != ChouetteVelute && combiMise != SiropGrelot ) {
				combiMise = annonceCombinaison();
			}
				 
			unsigned int mise = 0;
			cout << "Mise : "; mise = safe_get_int();
			
			jeu.lanceur = defie-1;
			Combinaison resultat = tour(jeu);
			
			bool reussi = defiReussi(combiMise, resultat);
			
			cout << endl;
			if(reussi) {
				cout << "Défi Grelottine réussi ! " << endl;
				donnerPts(jeu, defie-1, mise);
				donnerPts(jeu, grelottin-1, -mise);
			} else {
				cout << "Défi Grelottine raté ! " << endl;
				donnerPts(jeu, defie-1, -mise);
				donnerPts(jeu, grelottin-1, mise);
			}
			jeu.joueurs[grelottin-1].grelottine = false;
		}
	}
	
	jeu.grelottineEnCours = false;
}

/*
 * Lance-Civet. Note : peut aussi être utilisé pour rectifier qui possède un civet ou pas.
 */
 
void lance_civet(Jeu& jeu) {
	cout << "Qui lance son civet ? ";
	unsigned int depart = designer(jeu.joueurs.size());
	cout << "À qui ? ";
	unsigned int arrivee = designer(jeu.joueurs.size());
	
	if( depart != 0 && arrivee != 0 ) {
		jeu.joueurs[depart-1].civet = false;
		jeu.joueurs[arrivee-1].civet = true;
	}
}


//! FONCTIONS EN RAPPORT AVEC LES COMBINAISONS ############################################################
/*! Les fonctions suivantes testent si un résultat de dés est une certaine combinaison, et retournent la valeur dans le cas
 * échéant, sinon retournent 0. Attention : Si un résultat est à la fois plusieurs combinaisons, plusieurs de ces fonctions
 * retourneront une valeur non-nulle.
 */

/*
 * Vérifie si la combinaison est une Velute, et retourne la valeur de la Velute, sinon retourne 0.
 * Attention : cette fonction retoure une Velute même en cas de Chouette-Velute ou de Suite !
 */
 
int is_velute(Dice dice) {
	dice = sort(dice);
	
	if(dice[0]+dice[1]==dice[2])
		return dice[2];
	else
		return 0;
}

/*
 * Vérifie si la combinaison est une chouette, et retourne la valeur de la chouette, sinon retourne 0.
 * Attention : cette fonction retourne une chouette même en cas de Chouette-Velute ou de Cul-de-Chouette !
 */
int is_chouette(Dice dice) {	
	dice = sort(dice);
						
	if(dice[0]==dice[1])
		return dice[0];
	else if(dice[1]==dice[2])
		return dice[1];
	else return 0;
}

/*
 * Vérifie si la combinaison est un Cul-de-Chouette
 */
 
int is_cdc(Dice dice) {
	if(dice[0]==dice[1] && dice[1]==dice[2])
		return dice[0];
	else
		return 0;
}

/*
 * Vérifie si la combinaison est une Suite
 */
 
int is_suite(Dice dice) {
	dice = sort(dice);
	
	return (dice[1]==dice[0]+1 && dice[2]==dice[0]+2);
}

/*
 * Vérifie si le résultat n'est aucune des combinaisons ci-dessus, auquel cas c'est un Néant
 */

int is_neant(Dice dice) {
	return !is_suite(dice) && !is_velute(dice) && !is_chouette(dice) && !is_cdc(dice);
}

/*
 * Applique les effets divers et variés qu'une combinaison obtenue en fin de lancer peut avoir.
 */

Combinaison effet_combinaison(Jeu& jeu, Dice& dice) {
	unsigned int val;
	size_t lanceur = jeu.lanceur;
	Combinaison combi = Neant;
								
	if(is_suite(dice)) {					// En cas de Suite, cette règle s'applique toujours.
		unsigned int perdant = 0;						
		cout << "Grelotte ça picote ! Qui a été le plus lent ? ";
		perdant = designer(jeu.joueurs.size());
		if(perdant>0) {
			donnerPts(jeu, perdant-1, -10);
		}
		combi = Suite;
	} // Remarque : pas de "else" ici : la suite se superpose aux autres combinaisons sans problème.
	  // Ensuite énumération exaustive de cas mutuellements exclusifs (exemple, une chouette-velute ne se gère pas
	  // comme une chouette + une velute.
	
	if(is_cdc(dice)) {						// En cas de Cul-de-Chouette, le joueur gagne de toute façon ses points.
		if(!jeu.sirotageEnCours) {
			val = is_cdc(dice);
			cout << "Magninfique Cul-de-Chouette de " << val << " ! ";
			donnerPts(jeu,lanceur, 40+10*val);
		}
		combi = CulDeChouette;
	}
	
	else if(is_velute(dice) && !is_chouette(dice)) {		// Velute, mais Pas Chouette-Velute
		val = is_velute(dice);
		cout << "Mais c'est une très belle Velute de " << val << " ça ! ";
		donnerPts(jeu,lanceur,val*val*2);
		if(combi == Suite)
			combi = SuiteVelute;
		else
			combi = Velute;
	}
	
	else if(is_velute(dice) && is_chouette(dice)) {			// Chouette-Velute
		val = is_velute(dice);
		cout << "Pas mou le caillou ! Qui a été le plus rapide ? ";
		unsigned int gagnant = designer(jeu.joueurs.size());
		if(gagnant != 0) {
			cout << jeu.joueurs[gagnant-1].nom << " gagne " << val*val*2 << " pts." << endl;
			jeu.joueurs[gagnant-1].score += val*val*2;
		}
		combi = ChouetteVelute;
	}
	
	else if(is_chouette(dice)) {							// Chouette simple. Possibilité de siroter si pas déjà fait.
		if(!jeu.sirotageEnCours) {
			val = is_chouette(dice);
			cout << "C'est une belle Chouette de " << val << " ! " ;
			donnerPts(jeu,lanceur,val*val);
			cout << endl;
			sirotage(jeu,dice,val);
		}
		combi = Chouette;
	}
	
	else if(is_neant(dice)) {									// Néant (la condition devrait être redondante (ce qui ne signifie pas qu'elle peut blesser))
		cout << "Néant ! " << jeu.joueurs[jeu.lanceur].nom << " a une Grelottine. " << endl;
		jeu.joueurs[jeu.lanceur].grelottine = true;
		combi = Neant;
	}
	
	return combi;
}	

/*!	TOUR DE JEU ###########################################################################################
 * 
 * 	Un tour de jeu, celui de jeu.joueur_actuel .
 * 	Si un joueur gagne la partie pendant ce tour, la fonction retourne le numéro du joueur gagnant,								// TODO (vraiment ?)
 * 		autrement elle retourne -1.
 * 
 */
Combinaison tour(Jeu& jeu) {
	cout << endl << "Lancer pour " << jeu.joueurs[jeu.lanceur].nom << endl;
	
	bool civet = false;
	Combinaison combiCivet;
	unsigned int miseCivet;
	if(jeu.joueurs[jeu.lanceur].civet) {															// Possibilité pour le lanceur d'utiliser un civet.
		cout << "Est-ce qu'il/elle utilise son civet (1/0) ? ";
		cin >> civet;
		
		if(civet) {
			jeu.civetEnCours = true;
			combiCivet = annonceCombinaison();
			cout << "et combien il mise (1-102) ? ";
			miseCivet = safe_get_int();
		}
	}
	
	
	cin.clear();
	cin.ignore(numeric_limits<streamsize>::max(), '\n');
	cout << endl << "Lancer des chouettes : ";
	Dice dice = {0,0,0};
	dice[0] = safe_get_int();
	dice[1] = safe_get_int();
	cout         << "Lancer du cul :        ";
	dice[2] = safe_get_int();
	dice = sort(dice);
	
	cout << endl;
	
	Combinaison result = effet_combinaison(jeu, dice);
	
	if(civet) {
		if(defiReussi(combiCivet, result)) {
			cout << "Civet réussi ! "; donnerPts(jeu,jeu.lanceur,miseCivet);
		} else {
			cout << "Civet raté ! "; donnerPts(jeu,jeu.lanceur,-miseCivet);
		}
		jeu.joueurs[jeu.lanceur].civet = false;
	}
	jeu.civetEnCours = false;
	
	return result;
}

/*
 * Vérifie si un joueur a gagné la partie en dépassant 343 pts, et dans ce cas retourne son identifiant.
 * ou si un joueur est éliminé en ayant moins de -343 pts, et dans ce cas le supprime du jeu.
 */

int check_gagnant(Jeu& jeu) {
	int max = -343;
	size_t Jmax = 0;
	int S = 0;
	
	for(size_t i=0; i<jeu.joueurs.size(); i++) {
		S = jeu.joueurs[i].score;
		if(S<=-343) {
			cout << jeu.joueurs[i].nom << " a " << S << " points, et est donc éliminé !" << endl;
			jeu.joueurs.erase(jeu.joueurs.begin()+i);
		}
	}
	for(size_t i=0; i<jeu.joueurs.size(); i++) {
		S = jeu.joueurs[i].score;
		if(S<=-343) {
			jeu.joueurs.erase(jeu.joueurs.begin()+i);
		} else if(S> max) {
			max = S;
			Jmax = i;
		}
	}
	
	if(max >= 343)
		return Jmax;
	else
		return -1;
}

//! MAIN ###################################################################################################

int main() {
	
	cout << " CUL DE CHOUETTE " << endl << endl;
	
	vector<Player> joueurs = demander_joueurs();						// Initialisation des joueurs
	Jeu jeu = {joueurs, 0, 0,false,false,false};
	Jeu saveState = jeu;
	int gagnant = -1;
	
	while(gagnant == -1) {												// Boucle principale
		
		string act = "";
		while (act != "0" && gagnant == -1) {								// Actions entre 2 tours
			imprimer(jeu);
			
			cout << "Action spéciale (B: Bévue, C: correction, AN: annuler tour, G: Grelottine, LC: Lance-Civet, 0) ? ";
			cin >> act; 
			
			if(act == "B") {													// Bévue
				bevue(jeu);
			} else if(act == "C") {												// Correction 
				correction(jeu);
			} else if(act == "G") {												// Grelottine
				saveState = jeu;
				grelottine(jeu);
			} else if(act == "LC") {											// Lance-Civet
				lance_civet(jeu);
			} else if(act == "AN") {											// Annulation du dernier tour
				jeu = saveState;
			} else {
				act = "0";
			}
			
			gagnant = check_gagnant(jeu);
		}
		
		if(gagnant == -1) {
			saveState = jeu;
			jeu.lanceur = jeu.joueur_actuel;
			tour(jeu);														// Tour régulier de jeu.joueur_actuel
			joueur_suivant(jeu);
		
			gagnant = check_gagnant(jeu);
		}
	} 																	//Fin de la boucle principale
	
	cout << "Félicitations à " << jeu.joueurs[gagnant].nom << " qui gagne la partie avec " << jeu.joueurs[gagnant].score << " points !" << endl;
	
	
	return 0;
}
	 // * Civet (fcts tour et civet).

     // Combinaisons : vérifier bonne gestion des cas mutuellement exclusifs (phase beta)
	 // Général : vérifier aucun risque de bug aux input (de nombres notamment)
	 // Fonctionnalité à ajouter : sauvegarder l'état du jeu après chaque tour dans un fichier
	 
	 // << Hé ben... On n'est pas sortis du sable ! >>
